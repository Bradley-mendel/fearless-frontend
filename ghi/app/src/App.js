import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Nav from './Nav';
import AttendeeForm from './AttendeeForm';
import AttendeesList from './AttendeesList';
import ConferenceForm from './ConferenceForm';
import LocationForm from './LocationForm';
import MainPage from './MainPage';
import PresentationForm from './PresentationForm';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
              <Route path="attendees/new" element={<AttendeeForm />} />
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
          <Route index element={<MainPage />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

